#!/bin/bash
echo "Stopping Bot Restarter..."
ps aux | grep bot_restarter | awk '{print $2}' | xargs kill &
sleep 1
echo "Stopping Bot Processes..."
ps aux | grep bot.py | awk '{print $2}' | xargs kill &
sleep 1
echo "Bot Removal Complete"

exit 0
