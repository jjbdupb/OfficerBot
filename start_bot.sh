#!/bin/bash
echo "Starting Bot...."
python3 bot.py &
sleep 1
echo "Bot started as background process"
sleep 1
echo "Starting Bot Restarter...."
/bin/bash bot_restarter.sh &
sleep 1
echo "Bot Restarter Started as background process"
sleep 1
exit 0
