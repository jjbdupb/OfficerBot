#!/bin/bash
set -e 
set -x 

cd /opt/OfficerBot/
sudo ./stop_bot.sh
sudo git pull
sudo ./start_bot.sh &
exit 0
