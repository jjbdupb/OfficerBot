##############################################################################
#  FILE
#      $HeadURL: https://gitlab.com/jjbdupb/OfficerBot/blob/master/bot.py
#      $Author: jdpb
#
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Main Python file for the officer bot of the guild Valued <Kazzak>
#      Deployment is in AWS instance ask Josh about it if unsure
#
##############################################################################

###########
# Imports #
###########

import discord
import configparser
import asyncio
import re
import logging
import json
import logging.handlers
from Bot_Sheet_API import Spreadsheet_API
from datetime import datetime


####################
# Configure Config #
####################

config = configparser.ConfigParser()
config.read('bot.conf')
config = config["Bot_Conf"]
TOKEN = config['Token']
officer_msg_id = config.getint('OfficerMsgID')
lim = config.getint('CleanLimit')
clean_time = config.getint('CleanTime')
timeout_time = config.getint('TimeoutTime')
bot_check_time = config.getint('BotCheckTime')
joshID = config.getint('JoshID')
EventsID = config.getint('EventsID')
bot_channel_id = config.getint('BotChanID')
application_channel_id = config.getint('ApplicationChanID')
argument_match = re.compile('"(.*?)"')

#################
# setup logging #
#################

logging_level = config['LoggingLevel']
logger = logging.getLogger('discord')
if logging_level == "DEBUG":
    logger.setLevel(logging.DEBUG)
else:
    logger.setLevel(logging.INFO)

handler = logging.handlers.TimedRotatingFileHandler(
    filename='/var/log/bot/bot.log',
    encoding='utf-8', when="midnight", backupCount=3)
handler.setFormatter(logging.Formatter(
    '%(asctime)s:%(levelname)s:%(name)s: %(message)s'))
logger.addHandler(handler)

client = discord.Client()


############################################################
# Function called whenever a message is sent on the server #
############################################################
@client.event
async def on_message(message):
    # Check if the new message is from the bot
    if message.author == client.user:
        # if the bot message is in the events channel
        # add initial reactions
        if message.channel == client.get_channel(EventsID):
            await message.add_reaction(u"\u2705")
            await message.add_reaction(u"\u274C")
            return
        # Ignore all other bot created messsages
        else:
            return
    # Check if message is in bot_channel - This allows use of commands
    if message.channel == client.get_channel(bot_channel_id):
        # Check what commands are issued

        # clean removes all messages from officer_message that
        # occured in the last day
        if (message.content.startswith("!clean")):
            channel = client.get_channel(officer_msg_id)
            await channel.purge_from(channel, limit=lim, before=(
                datetime.today() - datetime.timedelta(clean_time)))
            return

        # Purge command clears the officer_message channel of all messages
        elif (message.content.startswith("!purge")):
            channel = client.get_channel(officer_msg_id)
            await channel.purge_from()
            return

        # update logs command, takes data from the spreadsheet
        # mainly what names and specs people are playing
        # and uses this with the warcraftlogs api
        # to correctly, and automaticaly update logs
        # uses gspread and oauth2 to function
        elif (message.content.startswith("!updatelogs")):
            await message.channel.send("Updating Logs This may take several mins" + (
                "Please Wait ....."))
            sheet = Spreadsheet_API()
            try:
                sheet.update_logs()
            except Exception as e:
                logger.error("Unhandled Exception in update logs\n")
                logger.error("Error was: " + str(e))
                await message.channel.send("An Error occued updating logs")
                await message.channel.send("Try again and show josh this message: " + (
                    str(e)))
                return
            await message.channel.send("Logs Updated")
            return

        # New event command takes the input args to create a new
        # guild event, uses discord embeds for formatting
        elif message.content.startswith("!NewEvent"):
            channel = client.get_channel(EventsID)
            args = re.findall(argument_match, message.content)
            try:
                date = datetime.strptime(args[0], "%d/%m/%Y")
            except Exception as e:
                await message.channel.send("Error in Date object" + str(e))
                return
            time = args[1]
            description = args[2]
            embed = discord.Embed(
                title="Raid " + datetime.strftime(date, "%d/%m/%y"),
                description="Going @ " + str(
                    time), color=0x00ecff)
            embed.set_author(name=message.author.name)
            embed.set_thumbnail(
                url="https://lggaming.net/wp-content/uploads/2018/06/uldir-raid-testing-june-14-18-heroic-and-normal-768x316.jpg")

            embed.add_field(
                name="---------", value=description, inline=False)
            embed.add_field(
                name="---------",
                value="to signup click one of the reactions below",
                inline=True)
            embed.set_footer(
                text="Any issue with this bot tell Josh")
            await channel.send(embed=embed)

            return

        # List command, takes given arguments, (event id, and list to check)
        # returns a list of all people who reacted with the given responce to
        # the event with the corrisponding ID
        elif message.content.startswith("!list"):
            args = re.findall(argument_match, message.content)
            wrong_id = 0
            try:
                messag_id = int(args[0])
                reaction_type = args[1]
            except IndexError:
                await message.channel.send(
                    "Event ID or Reaction Type could not be found in your command..."+
                    " Please check your formatting")
                return
            with open("events.json", "r+") as json_file:
                data = json.load(json_file)
                if reaction_type.lower() == 'no':
                    await message.channel.send(
                        "Generating list of People Not attending Event " + (
                            str(messag_id)))
                    for p in data['No']:
                        if p['id'] == messag_id:
                            for raider in p['People'].keys():
                                await message.channel.send(
                                    raider + (
                                        " | reacted at ") + (
                                        p['People'][raider]))
                        else:
                            wrong_id += 1
                        if wrong_id >= (len(data['No'])):
                            await message.channel.send(
                                " ** Event ID Could not be found ... \nDo you have the right ID? **")
                            return
                elif reaction_type.lower() == 'yes':
                    await message.channel.send(
                        "Generating list of People attending Event " + (
                            str(messag_id)))
                    for p in data['Yes']:
                        if p['id'] == messag_id:
                            for raider in p['People'].keys():
                                await message.channel.send(
                                    raider + (
                                        " | reacted at ") + (
                                        p['People'][raider]))
                        else:
                            wrong_id += 1
                        if wrong_id >= (len(data['Yes'])):
                            await message.channel.send(
                                " ** Event ID Could not be found ... \nDo you have the right ID? **")
                            return
                else:
                    await message.channel.send(
                        "Reaction Type Not Recognised ..." +
                        ' Please use the following format\n' +
                        '!list "<EventID>"" "<Yes/No>"')
                    return
            await message.channel.send("** Finished Generating List **")
            return

        # Checks who with the roll @raider has not responded to the given
        # event, by using the event id
        elif message.content.startswith("!NoReact"):
            reaction_list = []
            wrong_id = 0
            messag_id = int(re.findall(argument_match, message.content)[0])
            with open("events.json", "r+") as json_file:
                data = json.load(json_file)
                for p in data['Yes']:
                    if p['id'] == messag_id:
                        for i in p['People'].keys():
                            reaction_list.append(i)
                    else:
                        wrong_id += 1
                for N in data['No']:
                    if N['id'] == messag_id:
                        for j in N['People'].keys():
                            reaction_list.append(j)
                    else:
                        wrong_id += 1
            if wrong_id >= (len(data['No']) + len(data['Yes'])):
                await message.channel.send(
                    " ** Event ID Could not be found ... \nDo you have the right ID? **")
                return
            await message.channel.send("Finding People Not Reacted.... :")
            for member in client.get_all_members():
                if 'Raider' in [y.name for y in member.roles]:
                    if member.name not in reaction_list:
                        await message.channel.send(member.name)
            await message.channel.send("Found all People not Reacted")
            return
    # if the message is in a PM, this is a PM to the bot,
    # Thus the message will be forwared to the officer message channel
    if isinstance(message.channel, discord.abc.PrivateChannel):
        # any messages is sent that is not from an officer,
        # is assumed to be a message ment for the officers
        # and is forwarded to the officer message channel
        channel = client.get_channel(officer_msg_id)
        msg = str(message.author) + " Sent: " + message.content + "\n"
        await channel.send(msg)


# Function Called on a reaction removed,
# gives raw http payload data
@client.event
async def on_raw_reaction_remove(payload):

    # Get the bassic info and models from the http payload
    user = client.get_user(payload.user_id)
    channel = client.get_channel(payload.channel_id)
    emoji = payload.emoji.name

    # if the user is the bot, ignore
    if user == client.user:
        return
    # Logic is only inplemented if the reaction is in the events chanel
    if channel == client.get_channel(EventsID):
        # if the emoji removed is the X, correct json data and notify officers
        if emoji == u"\u274C":
            chan = client.get_channel(officer_msg_id)
            await chan.send(user.name +
                            " Removed Their X reaction for Event " +
                            str(payload.message_id))
            with open("events.json", "r+") as json_file:
                data = json.load(json_file)
                for p in data['No']:
                    if p['id'] == payload.message_id:
                        user = client.get_user(payload.user_id)
                        del p['People'][user.name]
                json_file.seek(0)
                json.dump(data, json_file, indent=4)
                json_file.truncate()

        # If the emoji removed is the Tick,
        # Correct json data and notify officers
        elif emoji == u"\u2705":
            chan = client.get_channel(officer_msg_id)
            await chan.send(user.name +
                            " Removed Their Tick reaction for Event " +
                            str(payload.message_id))
            with open("events.json", "r+") as json_file:
                data = json.load(json_file)
                for p in data['Yes']:
                    if p['id'] == payload.message_id:
                        user = client.get_user(payload.user_id)
                        del p['People'][user.name]
                json_file.seek(0)
                json.dump(data, json_file, indent=4)
                json_file.truncate()


# Function Called reacton add
# gives raw http payload data
@client.event
async def on_raw_reaction_add(payload):
    # Get basic info and moddles from raw payload data
    user = client.get_user(payload.user_id)
    channel = client.get_channel(payload.channel_id)

    # if the user is the bot, ignore
    if user == client.user:
        return

    # logic is only inplemented for events channel
    if channel == client.get_channel(EventsID):

        # if the emoji is not tick or X, remove the reaction
        if payload.emoji.name != u"\u2705" and payload.emoji.name != u"\u274C":
            await client.http.remove_reaction(
                payload.message_id, payload.channel_id,
                payload.emoji, payload.user_id)

        else:
            # Else if emoji is X, notify officers and record in json data
            if payload.emoji.name == u"\u274C":
                chan = client.get_channel(officer_msg_id)
                await chan.send(user.name +
                                " Reacted with a X for Event " +
                                str(payload.message_id))
                with open("events.json", "r+") as json_file:
                    no_mtch_cnt = 0
                    timestamp = datetime.now()
                    data = json.load(json_file)
                    for p in data['No']:
                        if p['id'] == payload.message_id:
                            p['People'].update({user.name: timestamp})
                        else:
                            no_mtch_cnt += 1
                    if no_mtch_cnt >= len(data['No']):
                        data['No'].append(
                            {"id": payload.message_id, "People": {
                                user.name: timestamp}})
                    json_file.seek(0)
                    json.dump(data, json_file, indent=4, default=str)
                    json_file.truncate()
            # else if emoji is Tick, record in json data
            # but don't bother officers with this info
            elif payload.emoji.name == u"\u2705":
                with open("events.json", "r+") as json_file:
                    no_mtch_cnt = 0
                    timestamp = datetime.now()
                    data = json.load(json_file)
                    for p in data['Yes']:
                        if p['id'] == payload.message_id:
                            p['People'].update({user.name: timestamp})
                        else:
                            no_mtch_cnt += 1
                    if no_mtch_cnt >= len(data['Yes']):
                        data['Yes'].append(
                            {"id": payload.message_id, "People": {
                                user.name: timestamp}})
                    json_file.seek(0)
                    json.dump(data, json_file, indent=4, default=str)
                    json_file.truncate()


############################
# Called when bot is ready #
############################
@client.event
async def on_ready():
    logger.info('-------------------------------')
    logger.info('Logged in as')
    logger.info("Username: " + str(client.user.name))
    logger.info("ID: " + str(client.user.id))
    logger.info('-------------------------------')

    # check aplications function, uses gspread api to check
    # apliations from the google form
    # if any new aplications are found will send a message in officer message
    if config.getboolean('CheckApplications') is True:
        sheet_interface = Spreadsheet_API()
        channel = client.get_channel(application_channel_id)
        await channel.send("** Application Checker Online ... **")
        while True:
            try:
                logger.debug("Checking for New Application ...")
                response, battle_tag = sheet_interface.Check_New_Application()
                if response is True:

                    found_applicant = ("** A new Application Has been ") + (
                        "Subbmited from ") + battle_tag + (
                        " Please Check the response found at ** \n") + (
                        "https://docs.google.com/forms/d/11d83XUnHIe_079d5V9Wl53gJzyyxJ5Ywezi9LH-ARfg/edit#responses")

                    logger.debug("New Application Found Sending Message ...")
                    await channel.send(found_applicant)
                    logger.debug("Message Sent")
                else:
                    logger.debug("No New Application Found")
                await asyncio.sleep(bot_check_time)
            except Exception as e:
                logging.error(str(e))
                pass
client.run(TOKEN)
