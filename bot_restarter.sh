#!/bin/bash
while true; do
	if ! pgrep -f "bot.py" > /dev/null; then
		python3 bot.py &
		timestamp="`date +%Y/%m/%d/%H-%M-%S`"
		echo " $timestamp ERROR Bot had to be restarted ..." >> /var/log/bot/bot.log
	fi
	sleep 10
done 
