##############################################################################
#  FILE
#      $HeadURL: gitlab.com/jjbdupb/OfficerBot/blob/master/Bot_Sheet_API.py
#      $Author: jdpb
#
#  ORIGINAL AUTHOR
#      Joshua du Parc Braham
#
#  DESCRIPTION
#      Python File that acts as an interface between the officerbot and
#      google sheets, uses gspread libary through oauth2
#      Ask josh if unsure how parts work
#
##############################################################################

###########
# Imports #
###########

import gspread
import re
import time
import requests
import logging
import configparser
from oauth2client.service_account import ServiceAccountCredentials

############################################
# Connect to Google Sheets using json file #
############################################

scope = ['https://spreadsheets.google.com/feeds',
         'https://www.googleapis.com/auth/drive']


# Main class of file, contains all connections for spreadsheet #
class Spreadsheet_API():

    def __init__(self):
        self.config = configparser.ConfigParser()
        self.config.read('bot.conf')
        self.config = self.config["Bot_Conf"]
        self.token_refresh_time = self.config.getint('TokenRefreshTime')
        self.healer_list = ["Holy Pal", "Mistweaver",
                            "Resto Sham", "Resto druid", "Holy", "Disc"]
        self.re_match = re.compile('[\w]')
        self.Connect_to_sheets()
        self.Filled_Cells = self.application_sheet.findall(self.re_match)
        self.token_time = time.time()
        self.logger = logging.getLogger('discord')
        self.roll_col = self.config.getint('SheetRosterRoll')
        self.col_ids = {2128: 4, 2134: 6, 2135: 8,
                        2136: 5, 2141: 3, 2144: 2, 2145: 7, 2122: 9}

    # Uses oauth2 to connect to google sheets api, token needs to be refreshed
    def Connect_to_sheets(self):
        creds = ServiceAccountCredentials.from_json_keyfile_name(
            'client_secret.json', scope)
        client = gspread.authorize(creds)
        self.application_sheet = client.open(
            "Valued Application Form (Responses)").sheet1
        self.logs_sheet = client.open("BFA Master Sheet")

    # refreshes oauth2 token every hour, done by connect_to_sheets
    # function if an hour has passed
    def refresh_oauth2_token(self):
        if (time.time() - self.token_time) > self.token_refresh_time:
            self.logger.info("Refreshing oauth2 Token ...")
            self.token_time = time.time()
            self.Connect_to_sheets()
            self.logger.info("oauth2 Token Refreshed")

    # checks to see if there has been a change in the aplication spreadsheet
    # if a change is detected reutrns true, and updates cached version of sheet
    def Check_New_Application(self):
        self.refresh_oauth2_token()
        self.cell_list = self.application_sheet.findall(self.re_match)

        if str(self.cell_list) != str(self.Filled_Cells):
            self.Filled_Cells = self.cell_list
            battle_tag = str((self.application_sheet.col_values(
                21,
                value_render_option="UNFORMATTED_VALUE"))[-1])
            return True, battle_tag
        else:
            return False, None

    # updates the logs of all players in guild to bfa sheet
    # uses warcraftlogs api to do this, called by bot on !updatelogs command
    def update_logs(self):
        self.refresh_oauth2_token()

        self.CurrentRosterSheet = self.logs_sheet.worksheet("Roster")
        self.CurrentHCLogsSheet = self.logs_sheet.worksheet("Performance - HC")
        self.CurrentMythicLogsSheet = self.logs_sheet.worksheet(
            "Performance - Mythic")

        self.Get_current_roster()
        for i in self.roster_names:
            self.hc_char_data = {2128: [], 2134: [],
                                 2135: [], 2136: [],
                                 2141: [], 2144: [],
                                 2145: [], 2122: []}
            self.mythic_char_data = {2128: [], 2134: [],
                                     2135: [], 2136: [],
                                     2141: [], 2144: [],
                                     2145: [], 2122: []}
            _row = self.roster_names.index(i) + 2
            if i in self.healers:
                metric = "hps"
            else:
                metric = "dps"

            api = "https://www.warcraftlogs.com:443/v1/parses/character/" + (
                i + "/Kazzak/EU?metric=" + metric +
                "&timeframe=historical&api_key=" +
                "d705e44847784981fc848b98967936d8")
            all_data = requests.get((api), timeout=10).json()
            for q in range(0, len(all_data)):
                try:
                    fight_data = all_data[q]
                except KeyError:
                    self.logger.info("KeyError ..." + str(i) +
                                     "character Not found on WCL")
                    break
                for _id in self.hc_char_data.keys():
                    if fight_data['encounterID'] == _id and fight_data['difficulty'] == 4:
                        percentile = (
                            (1 - (
                                fight_data['rank'] /
                                fight_data['outOf'])) * 100)
                        self.hc_char_data[_id].append(percentile)

                    if fight_data['encounterID'] == _id and fight_data['difficulty'] == 5:
                        percentile = (
                            (1 - (
                                fight_data['rank'] /
                                fight_data['outOf'])) * 100)
                        self.mythic_char_data[_id].append(percentile)
            for _id in self.hc_char_data.keys():
                while True:
                    try:
                        myth_data = max(self.mythic_char_data[_id])
                        self.logger.debug(str(i) + ":" + str(_id) + str(
                            myth_data))
                        self.CurrentHCLogsSheet.update_cell(
                            _row, self.col_ids[_id],
                            max(self.hc_char_data[_id]))
                        self.CurrentMythicLogsSheet.update_cell(
                            _row, self.col_ids[_id],
                            myth_data)
                        break
                    except ValueError:
                        self.logger.info("Empty list, no data for that boss")
                        break
                    except Exception as e:
                        if __name__ == "__main__":
                            print("Hit api use limmit waiting ...")
                        self.logger.info(
                            "API Uses reached, waiting 130s then continueing")
                        time.sleep(130)
                        pass

    # gets current roster from bfa sheet roster section
    # called by update_logs
    # to get current roster to update logs for
    def Get_current_roster(self):
        self.roster_names = self.CurrentRosterSheet.col_values(
            1, value_render_option='UNFORMATTED_VALUE')
        self.roster_names = self.roster_names[1:]
        roster_spec = self.CurrentRosterSheet.col_values(
            self.roll_col, value_render_option='UNFORMATTED_VALUE')
        roster_spec = roster_spec[1:]
        roster_roll = []
        for i in roster_spec:
            if i not in self.healer_list:
                roster_roll.append("dps")
            else:
                roster_roll.append("hps")
        indices = [i for i, x in enumerate(roster_roll) if x == "hps"]
        self.healers = [self.roster_names[i] for i in indices]


if __name__ == "__main__":
    spread = Spreadsheet_API()
    spread.update_logs()
